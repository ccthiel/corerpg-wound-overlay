# CoreRPG Wound Overlay for FGU
A little diddy to make things mo betta when it comes to combat in Fantasy Grounds. It doesn't do much, but what it does I find super handy and will work with any ruleset using the CoreRPG ruleset.

[Download](https://gitlab.com/ccthiel/CoreRPG-Wound-Overlay/-/jobs/artifacts/latest/file/CoreRPG-Wound-Overlay.ext?job=build-extension) the latest version of the plugin!

[Gitlab repository](https://gitlab.com/ccthiel/CoreRPG-Wound-Overlay)

[Fantasy Grounds Forum Thread](https://www.fantasygrounds.com/forums/showthread.php?66315-CoreRPG-Wound-Overlay&p=580803#post580803)

[Discord Channel](https://discord.com/channels/812870526455250945/812954688131432448)

Proud member of the [Fantasy Grounds Unofficial Developer's Guild!](https://gitlab.com/fantasy-grounds-unofficial-developers-guild)

Much Thanks To:
Kelrugem, Celestian, damned, Moon Wizard, tahl_liadon for answering my many questions and providing code, graphics, guidance, etc.

# Additional Contributors
These folks have contributed to the extension through merge requests:
* Good King Enialb

Features:
  * All: Wound overlays for tokens based upon their health status

Please see [Bug Reporting](https://gitlab.com/ccthiel/CoreRPG-Wound-Overlay/-/blob/main/BUG_REPORT.md) when it comes to filing a bug. I'd absolutely weclome people reporting bugs (or even forking the repository and submitting a merge request!), but please do a bit of work on your part to make my life easier. Thank you! 

Want to become a member of the Fantasy Grounds Unofficial Developers Guild? [Join up!](https://discord.gg/yAXPgR8Bc8) It'll give you [developer](https://docs.gitlab.com/ee/user/permissions.html) access to this repository (and hopefully others soon!)
