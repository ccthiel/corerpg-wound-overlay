
function onInit()
	UDGCoreRPGWoundOverlayHelper.verbose({"campaign/scripts/imagewindow_toolbar::oninit()"});
	update(true);
	if super and super.onInit then
		super.onInit();
	end
end

function getImage()
	return parentcontrol.window.image;
end

function update()
	UDGCoreRPGWoundOverlayHelper.verbose({"campaign/scripts/imagewindow_toolbar::update()"});
	local image = getImage();
	local bHasTokens = image.hasTokens();

	if Session.IsHost and bHasTokens then
		toolbar_clear_wounds.setVisible(true);
		clear_wound_separator_left.setVisible(true);
		clear_wound_separator_right.setVisible(true);
	elseif Session.IsHost and not bHasTokens then
		toolbar_clear_wounds.setVisible(false);
		clear_wound_separator_left.setVisible(false);
		clear_wound_separator_right.setVisible(false);
	end

	if bHasTokens then
		UDGCoreRPGWoundOverlayTokenOverlayManager.updateAllWoundOverlays();
	end

	if super and super.update then
		super.update(bInit);
	end
end
