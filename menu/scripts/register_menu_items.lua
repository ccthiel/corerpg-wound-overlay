function onInit()		
	UDGCoreRPGWoundOverlayHelper.verbose({"register_menu_items.lua::onInit()"});
	registerMenuItems();		
end

function registerMenuItems() 	
	UDGCoreRPGWoundOverlayHelper.verbose({"register_menu_items.lua::registerMenuItems()"});

	OptionsManager.registerOption2("CORERPG_WOUND_OVERLAY_DEBUG", false, "menu_option_header_corerpg_wound_overlay", "menu_option_header_corerpg_wound_overlay_debug", "option_entry_cycler", { labels = "option_val_off|option_val_debug|option_val_verbose", values = "off|debug|verbose", default = "off" });

end
